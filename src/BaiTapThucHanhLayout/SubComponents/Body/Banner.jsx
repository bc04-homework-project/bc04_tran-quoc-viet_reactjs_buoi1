import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div className="py-5">
          <div className="container px-lg-5">
            <div className="p-4 p-lg-5 bg-light rounded-3 text-left">
              <div className="m-3 m-lg-4">
                <h1 className="display-3 fw-bold">A warm welcome!</h1>
                <p className="fs-4">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Facilis voluptates aspernatur beatae rem asperiores fuga
                  numquam ea! Nobis inventore error illo nisi tempora, quae
                  excepturi laudantium cum, nemo culpa quia?
                </p>
                <a className="btn btn-primary btn-lg" href="#!">
                  Call to action
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
