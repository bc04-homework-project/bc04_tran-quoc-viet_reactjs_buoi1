import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";

export default class Body extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <Banner />
          <div className="container row pl-lg-5">
            <Item />
            <Item />
            <Item />
            <Item />
          </div>
        </div>
      </div>
    );
  }
}
