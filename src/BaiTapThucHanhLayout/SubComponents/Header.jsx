import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="bg-dark">
        <nav className="container navbar navbar-dark">
          <div className="container px-lg-5">
              <a className="navbar-brand" href="#!">
                Start Bootstrap
              </a>

              <ul className="nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="#!">
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#!">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#!">
                    Services
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#!">
                    Contact
                  </a>
                </li>
              </ul>
          </div>
        </nav>
      </div>
    );
  }
}
