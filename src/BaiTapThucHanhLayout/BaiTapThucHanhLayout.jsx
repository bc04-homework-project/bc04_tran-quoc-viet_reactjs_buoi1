import React, { Component } from 'react'
import Body from './SubComponents/Body/Body'
import Footer from './SubComponents/Footer'
import Header from './SubComponents/Header'

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
      
      <div>
          <Header/>
          <Body/>
          <Footer/>
      </div>
      
      </div>
    )
  }
}
